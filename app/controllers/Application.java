package controllers;

import play.*;
import play.mvc.*;
import java.util.*;
import org.apache.commons.lang.StringUtils;
import models.*;

public class Application extends Controller {

  public final static int itemsPerPage = 10;

  public static void index(String page) {
    int pageNumber = 1;
    long count = Link.count();
    long pages = (count / itemsPerPage) + 1;
    try {
      pageNumber = Integer.parseInt(page);
    } catch (Exception e) {
      Logger.warn(e, "%s is not a number", page);
    }
    List<Link> links =
      Link.find("order by ts desc").fetch(pageNumber, itemsPerPage);
    render(links, pages);
  }

  public static void publish(String author, String url, String remarks) {
    String validatedUrl = "";
    validation.required(author);
    validation.required(url);
    if (validation.hasErrors()) {
      params.flash();
      validation.keep();
    } else {
      validatedUrl = StringUtils.removeStart(StringUtils.removeStart(url, "https://"), "http://");
      Link newLink = new Link(author, validatedUrl, remarks);
      newLink.save();
    }
    index("1");
  }

  public static void delete(Long id) {
    Link target;
    Logger.info("delete id = ", id);
    if (id != null) {
      target = Link.findById(id);
      if (target != null) {
        target.delete();
      }
    }
    index("1");
  }
}
