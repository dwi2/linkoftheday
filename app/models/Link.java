package models;

import play.*;
import play.db.jpa.*;
import play.data.validation.*;
import java.util.*;
import javax.persistence.*;

@Entity
public class Link extends Model {

  @Required
  public String url;

  @Required
  public Long ts;

  @Required
  public String author;

  public String remarks;

  public Link(String author, String url, String remarks) {
    // TODO: regex check on url ?
    this.url = url;
    this.author = author;
    this.ts = Calendar.getInstance().getTimeInMillis();
    this.remarks = remarks;
  }

}